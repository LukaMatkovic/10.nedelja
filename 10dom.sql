-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 13, 2018 at 10:46 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `10dom`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

DROP TABLE IF EXISTS `ads`;
CREATE TABLE IF NOT EXISTS `ads` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `adname` varchar(50) CHARACTER SET utf8 NOT NULL,
  `description` text NOT NULL,
  `image` varchar(100) CHARACTER SET utf8 NOT NULL,
  `price` float NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `adname`, `description`, `image`, `price`, `date`) VALUES
(4, 'Jagode', 'organske sveze, cena po kg', 'img/ads/strawberries.jpg', 250, '2018-04-13 20:19:53'),
(2, 'Nemacki ovcar - stene', 'mladunce nemackog ovcara, star 5 nedelja\r\nmuski', 'img/ads/puppy.jpg', 6000, '2018-04-13 17:12:14'),
(3, 'Danilo Kis - Basta pepeo', 'Najpoznatiji roman subotičkog pisca izdat 1965. godine. Polovno izdanje, dobro sačuvano.', 'img/ads/bastapepeo.jpg', 300, '2018-04-13 17:31:58'),
(5, 'Kacey Musgraves - Golden Hour (Vinyl)', 'An excellent new album from Grammy Award-winning Texan country superstar now available on vinyl!', 'img/ads/golden hour.jpg', 2100, '2018-04-13 22:44:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(3) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `email`, `firstname`, `lastname`) VALUES
(1, 'user1', 'e820590dd4cc57baa8348b8f905d74b2', 'user@user.com', 'Luka', 'Matkovic');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
