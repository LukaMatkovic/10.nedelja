<?php
    session_start();

    require("inc/config.php");
    require("inc/db.php");
    require("inc/functions.php");


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Sistem za unos oglasa">
        <meta name="keywords" content="MySQL, form, PHP, oglas">
        <meta name="author" content="Luka Matkovic">
        <meta name="viewport" content="width=device-width, initial-scale=1">
   
        <title>Domaci - Oglasi</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900" rel="stylesheet">

    </head>
    <body>
        <div class="wrapper">     
            <div class="header"><?php require "inc/header.php"; ?></div>
            <div class="aside"><?php require "inc/aside.php"; ?></div>
            <div class="main"><?php require "inc/main.php"; ?></div>
            <div class="footer"><?php require "inc/footer.php"; ?></div>
        </div>
    </body>
</html>