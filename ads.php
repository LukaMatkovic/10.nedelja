<?php
    session_start();

    require("inc/config.php");
    require("inc/db.php");
    require("inc/functions.php");


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Sistem za unos oglasa">
        <meta name="keywords" content="MySQL, form, PHP, oglas">
        <meta name="author" content="Luka Matkovic">
        <meta name="viewport" content="width=device-width, initial-scale=1">
   
        <title>Domaci - Oglasi</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900" rel="stylesheet">

        <style>
            * {
                font-family: 'Roboto', sans-serif;
                font-weight:400; 

            }
            .slika img {
                width:500px;
                height: auto;
            }
            .ime {
                font-weight: 800;
                font-size:20px;
                color:#0E4AF1;
            }
            p {
                margin: 15px;
            }
        
        </style>
    </head>
    <body>  
        <div class="wrapper">     
                <div class="header"><?php require "inc/header.php"; ?></div>
                <div class="aside"><?php require "inc/aside.php"; ?></div>
                <div class="main">

                <?php
                    $sql = "SELECT * FROM ads";
                    $result = mysqli_query($connection, $sql) or die(mysql_error());

                    if (mysqli_num_rows($result)>0) {

                        while($record = mysqli_fetch_array($result,MYSQLI_BOTH)) {
                            echo "<div class=\"slika\"><img src=\"".$record['image']."\"></div>";
                            echo "<div class=\"ime\">".$record['adname']."</div><br>";
                            echo "<p>".$record['description']."<br>";
                            echo "Cena: ".$record['price']." RSD<br>";
                            echo "Vreme postavljanja: ".$record['date']."</p>";
                        }
                    }
            ?>
            </div>
            <div class="footer"><?php require "inc/footer.php"; ?></div>
        </div>
    </body>
</html>