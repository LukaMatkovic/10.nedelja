<?php

    require("inc/config.php");
    require("inc/db.php");
    require("inc/functions.php");

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Sistem za unos oglasa">
        <meta name="keywords" content="MySQL, form, PHP, oglas">
        <meta name="author" content="Luka Matkovic">
        <meta name="viewport" content="width=device-width, initial-scale=1">
   
        <title>Domaci - Oglasi</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900" rel="stylesheet">

    </head>
    <body>
        <div class="wrapper">     
            <div class="headeradd"></div>
            <div class="form">
                <form action="realinsert.php" method="post" enctype="multipart/form-data">
                    <fieldset>

                        <legend>Insert New Ad</legend>
                        
                        <label for="adname">Name</label><br>
                        <input type="text" name="adname" required><br>

                        <label for="description">Description</label><br>
                        <textarea type="text" name="description" required rows="5" cols="50"></textarea><br>

                        <label for="file">Image</label><br>
                        <input type="file" name="file" required><br>

                        <label for="price">Price (RSD)</label><br>
                        <input type="number" name="price" required><br>

                        <input type="submit" name="submit" value="Insert"><br>
                        <a href="index.php">Return to Homepage</a>

                    </fieldset>
                </form>
            </div>
            <div class="footer"><?php require "inc/footer.php"; ?></div>
        </div>
    </body>
</html>



