<?php

    require("inc/config.php");
    require("inc/db.php");
    require("inc/functions.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $adname = $_POST["adname"];
    $description = $_POST["description"];
    $price = $_POST["price"];
}

if(isset($_FILES["file"]) AND is_uploaded_file($_FILES['file']['tmp_name'])) {
    $file_name = $_FILES["file"]["name"];
    $file_temp = $_FILES["file"]["tmp_name"];
    $file_size = $_FILES["file"]["size"];
    $file_type = $_FILES["file"]["type"];
    $file_error = $_FILES["file"]["error"];

    if($file_error > 0) {
        echo "Something went wrong during file upload!";
    }
    else {
        if(!exif_imagetype($file_temp)) {
            exit("File is not a picture!");
        }

        $ext_temp = explode(".", $file_name);
        $extension = end($ext_temp);

        $newfilename = $ext_temp[0].".$extension";
        $directory = "img/ads";
        $upload = "$directory/$newfilename";

        if(!is_dir($directory)) {
            mkdir($directory);
        }

        if(move_uploaded_file($file_temp,$upload)) {

        }
        else {
            echo "Error :(";
        }
    }
}

$sql = "INSERT INTO ads (adname, description, image, price, date)
        VALUES ('$adname', '$description', '$upload', '$price', CURTIME())";

$result = mysqli_query($connection, $sql) or die(mysql_error());

header("Location: index.php");
?>
